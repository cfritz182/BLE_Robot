################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../MSP_EXP432P401R_TIRTOS.cmd 

C_SRCS += \
../MSP_EXP432P401R.c \
../adc_joystick.c \
C:/ti/simplelink_msp432_sdk_bluetooth_plugin_1_25_00_42/source/ti/snp/cc2640r2lp_simple_np_uart_pm_sbl.c \
../gpiomain.c \
../hall.c \
../main_tirtos.c \
../pwm.c \
../simple_application_processor.c \
../simple_application_processor_params.c 

C_DEPS += \
./MSP_EXP432P401R.d \
./adc_joystick.d \
./cc2640r2lp_simple_np_uart_pm_sbl.d \
./gpiomain.d \
./hall.d \
./main_tirtos.d \
./pwm.d \
./simple_application_processor.d \
./simple_application_processor_params.d 

OBJS += \
./MSP_EXP432P401R.obj \
./adc_joystick.obj \
./cc2640r2lp_simple_np_uart_pm_sbl.obj \
./gpiomain.obj \
./hall.obj \
./main_tirtos.obj \
./pwm.obj \
./simple_application_processor.obj \
./simple_application_processor_params.obj 

OBJS__QUOTED += \
"MSP_EXP432P401R.obj" \
"adc_joystick.obj" \
"cc2640r2lp_simple_np_uart_pm_sbl.obj" \
"gpiomain.obj" \
"hall.obj" \
"main_tirtos.obj" \
"pwm.obj" \
"simple_application_processor.obj" \
"simple_application_processor_params.obj" 

C_DEPS__QUOTED += \
"MSP_EXP432P401R.d" \
"adc_joystick.d" \
"cc2640r2lp_simple_np_uart_pm_sbl.d" \
"gpiomain.d" \
"hall.d" \
"main_tirtos.d" \
"pwm.d" \
"simple_application_processor.d" \
"simple_application_processor_params.d" 

C_SRCS__QUOTED += \
"../MSP_EXP432P401R.c" \
"../adc_joystick.c" \
"C:/ti/simplelink_msp432_sdk_bluetooth_plugin_1_25_00_42/source/ti/snp/cc2640r2lp_simple_np_uart_pm_sbl.c" \
"../gpiomain.c" \
"../hall.c" \
"../main_tirtos.c" \
"../pwm.c" \
"../simple_application_processor.c" \
"../simple_application_processor_params.c" 


