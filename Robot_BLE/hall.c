#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>

/* Board Header file */
#include "Board.h"

/* Extern variables*/
extern pthread_mutex_t hold;
extern int length;
/* Privat variables*/
int hall_r_1_counter=0;
int hall_r_2_counter=0;
int hall_l_1_counter=0;
int hall_l_2_counter=0;

/*privat functions*/
void update_length();
int find_max();



void update_length(){
  length=find_max()/(700.0)*22; //in cm
  return;
}



int find_max(){
  int max_r=0;
  int max_l=0;
  max_r=(hall_r_1_counter>hall_r_2_counter)?hall_r_1_counter:hall_r_2_counter;
  max_l=(hall_l_1_counter>hall_l_2_counter)?hall_l_1_counter:hall_l_2_counter;
  return (max_r>max_l)?max_r:max_l;
}


void Fxn0(uint8_t index)
{
  hall_r_1_counter++;
}

void Fxn1(uint8_t index)
{
  hall_r_2_counter++;
}


void Fxn2(uint8_t index)
{
  hall_l_1_counter++;
}


void Fxn3(uint8_t index)
{
  hall_l_2_counter++;
}


void *hallThread(void *arg0)
{
  GPIO_setCallback(Board_GPIO_P2_3, Fxn0);
  GPIO_enableInt(Board_GPIO_P2_3);
  GPIO_setCallback(Board_GPIO_P5_1, Fxn1);
  GPIO_enableInt(Board_GPIO_P5_1);
  GPIO_setCallback(Board_GPIO_P3_5, Fxn2);
  GPIO_enableInt(Board_GPIO_P3_5);
  GPIO_setCallback(Board_GPIO_P3_7, Fxn3);
  GPIO_enableInt(Board_GPIO_P3_7);

  while (1) {
    usleep(1);
    update_length();
   }
}
