/*
 * Copyright (c) 2016-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== adcsinglechannel.c ========
 */
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/ADC.h>
#include <ti/display/Display.h>

/* Example/Board Header files */
#include "Board.h"

/* ADC sample count */
#define ADC_SAMPLE_COUNT  (10)

#define THREADSTACKSIZE   (768)

/* ADC conversion result variables */
uint16_t adcValue_x;
uint32_t adcValue_x_MicroVolt;
uint16_t adcValue_y;
uint32_t adcValue_y_MicroVolt;

static Display_Handle display;
extern uint_fast16_t STATEFLAG;

/*
 *  ======== threadFxn1 ========
 *  Open a ADC handle and get a array of sampling results after
 *  calling several conversions.
 */
void *threadFxn1(void *arg0)
{
      ADC_Handle   adc;
      ADC_Params   params;
      int_fast16_t res;
      uint32_t adc_Value_mean=0;
      int first=0;

      ADC_Params_init(&params);
      adc = ADC_open(Board_ADC1, &params);

      if (adc == NULL) {
          Display_printf(display, 0, 0, "Error initializing ADC channel 1\n");
          while (1);
      }
      while(1){
      /* Blocking mode conversion */
      res = ADC_convert(adc, &adcValue_y);

      if (res == ADC_STATUS_SUCCESS) {

          adcValue_y_MicroVolt = ADC_convertRawToMicroVolts(adc, adcValue_y);

          Display_printf(display, 0, 0, "ADC channel 1 convert result: %d uV\n", adcValue_y_MicroVolt);
      }
      else {
          Display_printf(display, 0, 0, "ADC channel 1 convert failed\n");
      }
      if(first==0){
                    first++;
                    adc_Value_mean=adcValue_y_MicroVolt;
                }


      if(adcValue_y_MicroVolt<adc_Value_mean-1000000){
          STATEFLAG=1;
      }
      else{
          STATEFLAG=0;
      }
      usleep(1000);
      }
  }

/*
 *  ======== mainThread ========
 */
void *adcThread(void *arg0)
{
    pthread_t           thread1;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    /* Call driver init functions */
    ADC_init();
    Display_init();

    /* Open the display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        /* Failed to open display driver */
        while (1);
    }

    Display_printf(display, 0, 0, "Starting the acdsinglechannel example\n");

    /* Create application threads */
    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    detachState = PTHREAD_CREATE_DETACHED;
            /* Set priority and stack size attributes */
            retc = pthread_attr_setdetachstate(&attrs, detachState);
            if (retc != 0) {
                /* pthread_attr_setdetachstate() failed */
                while (1);
            }
    pthread_attr_setschedparam(&attrs, &priParam);
    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }




    /* Create threadFxn1 thread */
    retc = pthread_create(&thread1, &attrs, threadFxn1, NULL);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    return (NULL);
}
