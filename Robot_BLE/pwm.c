
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>

/* Board Header file */
#include "Board.h"


extern uint_fast16_t STATEFLAG;
extern int angle;

/*Privat Variables*/
uint16_t   pwmPeriod = 3200;
PWM_Handle pwm1;
PWM_Handle pwm2;
PWM_Params params;

/*Privat Functions*/
void turn();
void set_Stop();
void set_Reverse();
void set_Forward();
void update_angle();



/* This function Stops the Robot, sets Reverse for some time and than turns
 * for 70 degrees */

void turn(){
  int i;
  set_Stop();
  set_Reverse(6);
  set_Reverse(7);
  PWM_setDuty(pwm1,PWM_DUTY_FRACTION_MAX/8);
  PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/8);
  for(i=0; i<10000000;i++){
   }
  set_Stop();
  for(i=0; i<1000000;i++){
   }
  PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/8);

  for(i=0; i<10000000;i++){
   }
  set_Forward(6);
  set_Forward(7);
  update_angle();
 }
/* Adds 70 degrees to current angle*/
void update_angle(){
  angle=((angle+70)>360)?(angle+70):(angle-290);
}
/* Stop the Motor*/
void set_Stop(){
  PWM_setDuty(pwm1,0);
  PWM_setDuty(pwm2,0);
}

/* Sets Pins LOW for Reverse*/
void set_Reverse(int j){
  if(j==6){
  GPIO_write(Board_GPIO_P2_6, 0);
   }
  else{
  GPIO_write(Board_GPIO_P2_7, 0);
   }
 }

/* Sets Pins HIGH for Reverse*/
void set_Forward(int j){
 if(j==6){
 GPIO_write(Board_GPIO_P2_6, 1);
  }
 else{
 GPIO_write(Board_GPIO_P2_7, 1);
  }
 }

/* PWM Thread*/
void *pwmThread(void *arg0)
{
 PWM_init();

  PWM_Params_init(&params);
  params.dutyUnits = PWM_DUTY_FRACTION;
  params.dutyValue = 0;
  params.periodUnits = PWM_PERIOD_US;
  params.periodValue = pwmPeriod;

  pwm1 = PWM_open(Board_PWM0, &params);
  if (pwm1 == NULL) {
  /* Board_PWM0 did not open */
    while (1);
   }
  PWM_start(pwm1);

  // open & start pwm 2
  pwm2 = PWM_open(Board_PWM1, &params);
  if (pwm2 == NULL) {
  /* Board_PWM0 did not open */
    while (1);
  }
  PWM_start(pwm2);
  set_Forward(6);
  set_Forward(7);

  while (1) {
    if(STATEFLAG==1){
      STATEFLAG=0;
      turn();
     }
    else{
      PWM_setDuty(pwm1,PWM_DUTY_FRACTION_MAX/4);
      PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/4);
      usleep(10000);
     }
  }
}
