#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
#include <math.h>
/* POSIX Header files */
#include <pthread.h>


/* Driver Header files */
#include <ti/drivers/GPIO.h>

/* Board Header file */
#include "Board.h"
#include "Profile/simple_gatt_profile.h"

extern  int x;
extern  int y;
extern  int x_old;
extern  int y_old;

extern  int length;
extern  int angle;

/*
 *  ======== mainThread ========
 */
void update_Position(){
x_old=x;
y_old=y;
x=x_old+length*sin(angle*0.0174533);
y=y_old+length*cos(angle*0.0174533);

SimpleProfile_SetParameter(SP_CHAR1_ID,sizeof(x),&x);
SimpleProfile_SetParameter(SP_CHAR2_ID,sizeof(y),&y);
}

void *mainThread(void *arg0)
{
    /* Call driver init functions */
    GPIO_init();

    while(1){
        update_Position();
    usleep(1000000);

    }

}

