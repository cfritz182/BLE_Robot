/*
 * Copyright (c) 2014-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*!*****************************************************************************
 *  @defgroup SBL Serial Bootloader Interface
 *  @brief This module implements an interface to the ROM based serial bootloader
 *         for the CC26xx. It can be used to load an image from an application
 *         processor to a CC26xx embedded device. The image may be either inside
 *         the application processor's internal memory, or in an external flash
 *         device.
 *         For more information see the Bootloader section of the
 *         [CC26xx Technical Reference Manual (TRM)](http://www.ti.com/lit/pdf/swcu117)
 *  @{
 *
 *  @file       sbl.h
 *  @brief      High level Serial Bootloader API
 *
 *  @ref SBL
 */

#ifndef SBL_H
#define SBL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include <stdint.h>

/*********************************************************************
 * CONSTANTS
 */

/** @defgroup SBL_INTERFACES SBL Interface types
 * @{
 */
#define SBL_DEV_INTERFACE_UART          0 //!< External device SBL set for UART
#define SBL_DEV_INTERFACE_SPI           1 //!< External device SBL set for SPI
#define SBL_DEV_INTERFACE_UNDEFINED     9 //!< External device SBL undefined
/** @} End SBL_INTERFACES */

/** @defgroup SBL_IMG_TYPES SBL Image types (internal/external)
 * @{
 */
#define SBL_IMAGE_TYPE_INT              0 //!< Image located in internal flash
#define SBL_IMAGE_TYPE_EXT              1 //!< Image located in external flash
/** @} End SBL_IMG_TYPES */

/** @defgroup SBL_RTN_VALUES SBL Return Values
 * @{
 */
#define SBL_SUCCESS                     0   //!< SBL procedure completed succesfully
#define SBL_DEV_ACK                     1   //!< SBL Target device returned ACK
#define SBL_DEV_NACK                    2   //!< SBL Target device returned NACK
#define SBL_FAILURE                     3   //!< SBL procedure failed
/** @} End SBL_RTN_VALUES */

/** @defgroup SBL_TARGET_PARMS Parameters set by the external flash device
 *            These values may need to be tweaked for custom hardware
 * @{
 */
#define SBL_MAX_TRANSFER                252     //!< Max block size for external flash SBL
#define SBL_PAGE_SIZE                   4096    //!< External flash page size
/** @} End SBL_TARGET_PARMS */

/*********************************************************************
 * TYPEDEFS
 */

/** @defgroup SBL_Structs SBL Stuctures
 * @{
 */
typedef struct
{
  uint8_t       targetInterface;          //!< SBL_DEV_INTERFACE_[UART,SPI]
  uint8_t       localInterfaceID;         //!< Device Interface (i.e. CC2650_UART0, CC2650_SPI0, etc.)
  uint32_t      resetPinID;               //!< Local PIN ID connected to target RST PIN
  uint32_t      blPinID;                  //!< Local PIN ID connected to target BL PIN
} SBL_Params;

typedef struct
{
  uint8_t       imgType;                //!< SBL_IMAGE_TYPE_[INT,EXT]
  uint32_t      imgInfoLocAddr;         //!< Address of image info header
  uint32_t      imgLocAddr;             //!< Local address of image to use for SBL
  uint32_t      imgTargetAddr;          //!< Target address to write image
} SBL_Image;

/** @} End SBL_Structs */

/*********************************************************************
*  EXTERNAL VARIABLES
*/
extern const SBL_Params SBL_defaultParams;

/*********************************************************************
 * FUNCTIONS
 */

/**
 * @brief   Initializes SBL parameters
 *
 * @param   params - SBL parameter structure to be set to SBL default values
 *
 * @return  None.
 */
extern void SBL_initParams(SBL_Params *params);

/**
 * @brief   Opens the SBL port for writing images
 *
 * @param   params - SBL parameters to initialize the port with
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_open(SBL_Params *params);

/**
 * @brief   Forces target device into SBL
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_openTarget( void );

/**
 * @brief   Exit SBL
 *
 * @return  None.
 */
extern void SBL_closeTarget( void );

/**
 * @brief   Forces target device to boot from flash image instead of SBL. This
 *          function can be called before SBL_open() or after SBL_open()
 *
 * @param   rstPinID - Board Pin ID of reset PIN
 * @param   blPinID  - Board Pin ID of boot loader PIN
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_resetTarget(uint32_t rstPinID, uint32_t blPinID);

/**
 * @brief   Writes image to the target device using SBL and resets device
 *
 * @param   image - parameters that describe the image to be written to target
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_writeImage(SBL_Image *image);

/**
 * @brief   Closes SBL port
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_close(void);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SBL_H */

/** @} End SBL */
