/*
 * Copyright (c) 2014-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*!*****************************************************************************
 *  @defgroup SBL_TL SBL Transport Layer
 *  @brief The SBL TL is a set of APIs used to send the image selected image
 *          to the network processor using serial data transfer. The TL is
 *          interface to the peripheral that will actually being sending the
 *          image on the wire.
 *  @{
 *
 *  @file       sbl_tl.h
 *  @brief      transport layer for sending images to NP
 *
 *  @ref SBL_TL
 */

#ifndef SBL_TL_H
#define SBL_TL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
*  EXTERNAL VARIABLES
*/

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/**
 * @brief   Open device port for communication with the target device. Currently
 *          only supports UART
 *
 * @param   pType - SBL_DEV_INTERFACE_[UART,SPI]
 * @param   pID - local serial interface ID (i.e. CC2650_UART0)
 *
 * @return  uint8_t - SBL_SUCCESS, SBL_FAILURE
 */
extern uint8_t SBL_TL_open(uint8_t pType, uint8_t pID);

/**
 * @brief   Get response message from the target device
 *
 * @param   pData - pointer to byte array to store data
 * @param   maxSize - size of byte array pointed to by pData
 * @param   len - will be set to the length of data written to pData
 *
 * @return  uint8_t - SBL_SUCCESS or SBL_FAILURE
 */
extern uint8_t SBL_TL_getRsp(uint8_t *pData, uint16_t maxSize, uint16_t *len);

/**
 * @brief   Sends a SBL command to target device
 *
 * @param   cmd - command ID
 * @param   pData - pointer to command payload
 * @param   len - length of command payload
 *
 * @return  uint8_t - SBL_SUCCESS
 */
extern uint8_t SBL_TL_sendCmd(uint8_t cmd, uint8_t *pData, uint16_t len);

/**
 * @brief   Send baud packet to allow target to auto detect
 *          baud rate of SBL transmissions
 *
 * @return  uint8_t - SBL_SUCCESS
 */
extern uint8_t SBL_TL_uartAutoBaud(void);

/**
 * @brief   Close interface to target device
 *
 * @return  None.
 */
extern void SBL_TL_close(void);

/**
 * @fn      SBL_TL_flushRxBuffer
 *
 * @brief   Flushes receive buffer. Only used for CC26xx SAP.
 *
 * @return  None.
 */
extern void SBL_TL_flushRxBuffer(void);


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SBL_TL */

/** @} End SBL_TL */
