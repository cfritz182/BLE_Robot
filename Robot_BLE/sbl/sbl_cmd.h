/*
 * Copyright (c) 2014-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*!*****************************************************************************
 *  @defgroup SBL_CMD Serial Bootloader Interface
 *  @brief These functions handle processing and packing the commands that can be
 *         sent to the ROM bootloader. These commands are used by the SBL layer to
 *         send the target image.
 *  @{
 *
 *  @file       sbl_cmd.h
 *  @brief      SBL Commands Interface
 *
 *  @ref SBL_CMD
 */


#ifndef SBL_CMD_H
#define SBL_CMD_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include <stdint.h>

/*********************************************************************
*  EXTERNAL VARIABLES
*/

/*********************************************************************
 * CONSTANTS
 */

/** @defgroup SBL_CMD_IDS SBL Commands IDs
 * @{
 */
#define SBL_CMD_PING                    0x20    //!< Ping the SBL Target Device
#define SBL_CMD_DOWNLOAD                0x21    //!< Prepares target for download
#define SBL_CMD_GET_STATUS              0x23    //!< Returns the status of the last command that was issued
#define SBL_CMD_SEND_DATA               0x24    //!< Transfers data and programs flash
#define SBL_CMD_RESET                   0x25    //!< Performs system reset
#define SBL_CMD_SECTOR_ERASE	        0x26    //!< Erases one sector within the flash main bank.
#define SBL_CMD_CRC32                   0x27    //!< Calculates CRC32 over a specified memory area.
#define SBL_CMD_GET_CHIP_ID             0x28    //!< Returns the 32-bit UserID from the AON_WUC JTAGUSERCODE register with MSB first.
#define SBL_CMD_MEMORY_READ             0x2A    //!< Reads a specified number of elements with a specified access width (8 bits or 32 bits)
#define SBL_CMD_MEMORY_WRITE            0x2B    //!< Currently not supported
#define SBL_CMD_BANK_ERASE              0x2C    //!< Performs an erase of all of the customer-accessible flash sectors not protected by FCFG1 and CCFG
#define SBL_CMD_SET_CCFG                0x2D    //!< Writes the CC26xx-defined CCFG fields to the flash CCFG area
/** @} End SBL_CMD_IDS */

/** @defgroup SBL_CMD_RET SBL Commands Return Values
 * @{
 */
//!< \brief Serial Bootloader Response IDs for CC26xx
#define SBL_CMD_RET_SUCCESS             0x40    //!< Status for successful command
#define SBL_CMD_RET_UNKNOWN_CMD         0x41    //!< Status for unknown command
#define SBL_CMD_RET_INVALID_CMD         0x42    //!< Status for invalid command (in other words, incorrect packet size)
#define SBL_CMD_RET_INVALID_ADR         0x43    //!< Status for invalid input address
#define SBL_CMD_RET_FLASH_FAIL          0x44    //!< Status for failing flash erase or program operation
/** @} End SBL_CMD_RET */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/**
 * @brief   The COMMAND_PING command receives an acknowledge from the bootloader,
 *          indicating that communication has been established. This command is
 *          a single byte.
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_ping(void);

/**
 * @brief   The COMMAND_DOWNLOAD command is sent to the bootloader to indicate
 *          where to store data in flash and how many bytes will be sent by the
 *          COMMAND_SEND_DATA commands that follow. The command consists of two
 *          32-bit values that are both transferred MSB first. The first 32-bit
 *          value is the address to start programming data into, while the second
 *          is the 32-bit size of the data that will be sent.
 *          This command must be followed by a COMMAND_GET_STATUS command to ]
 *          ensure that the program address and program size are valid for the
 *          device.
 *
 * @param   addr - Image address in internal flash
 * @param   size - Size of the image
 */
extern uint8_t SBL_CMD_download(uint32_t addr, uint32_t size);

/**
 * @brief   The COMMAND_GET_STATUS command returns the status of the last
 *          command that was issued.
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_getStatus(void);

/**
 * @brief   The COMMAND_SEND_DATA command must only follow a COMMAND_DOWNLOAD
 *          command or another COMMAND_SEND_DATA command, if more data is needed.
 *          Consecutive COMMAND_SEND_DATA commands automatically increment the
 *          address and continue programming from the previous location
 *
 * @param   pData - Pointer to data to send
 * @param   len   - length of buffer
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_sendData(uint8_t *pData, uint16_t len);

/**
 * @brief   The COMMAND_RESET command tells the bootloader to perform a system
 *          reset.
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_reset(void);

/**
 * @brief   The COMMAND_SECTOR_ERASE command erases a specified flash sector.
 *          One flash sector has the size of 4KB.
 *
 * @param   addr - start address of the sector to erase
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_sectorErase(uint32_t addr);

/**
 * @brief   The COMMAND_CRC32 command checks a flash area using CRC32.
 *
 * @param   addr - address in memory from where the CRC32 calculation starts
 * @param   size - number of bytes comprised by the CRC32 calculation
 * @param   readRepeatCnt - number of read repeats for each data location
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_crc32(uint32_t addr, uint32_t size, uint32_t readRepeatCnt);

/**
 * @brief   This command reads a specified number of elements with a specified
 *          access type (8- or 32 bits) from a specified memory mapped start
 *          address and returns the read data in a separate communication packet.
 *
 * @param   addr - address in memory from where the CRC32 calculation starts
 * @param   numAccess - number of 8 bit reads to do
 * @param   readRsp - pointer to buffer for read data
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_memoryRead(uint32_t addr, uint8_t numAccess, uint8_t *readRsp);

/**
 * @brief   The COMMAND_GET_CHIP_ID command makes the bootloader return the
 *          value of the 32-bit user ID from the AON_WUW JTAGUSERCODE register.
 *
 * @return  uint32_t - chipID
 */
extern uint32_t SBL_CMD_getChipID(void);

/**
 * @brief   This command erases all
 *          main bank flash sectors including CCFG not protected by write-protect
 *          bits in FCFG1 and CCFG
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_bankErase(void);

/**
 * @brief   The COMMAND_SET_CCFG command is sent to the bootloader to configure
 *          the defined fields in the lash CCFG area that are read by the ROM
 *          boot FW.
 *
 * @param   fieldID - which identifies the CCFG parameter to be written
 * @param   fieldValue   - data to write
 *
 * @return  uint8_t - status of TL return
 */
extern uint8_t SBL_CMD_setCCFG(uint32_t fieldID, uint32_t fieldValue);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* #define SBL_CMD_H */

/** @} End SBL_CMD */
