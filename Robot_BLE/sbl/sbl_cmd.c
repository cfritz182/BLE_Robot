/*
 * Copyright (c) 2014-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*********************************************************************
 * INCLUDES
 */
#include "sbl.h"
#include "sbl_cmd.h"
#include "sbl_tl.h"

#include <stdint.h>
#include <stdlib.h>

/*********************************************************************
 * CONSTANTS
 */

#define SBL_MAX_BYTES_PER_TRANSFER   252

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

/*********************************************************************
 * LOCAL FUNCTIONS
 */

static void SBL_CMD_uint32MSB(uint32_t src, uint8_t *pDst);

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/**
 * @fn      SBL_CMD_ping
 *
 * @brief   The COMMAND_PING command receives an acknowledge from the bootloader,
 *          indicating that communication has been established. This command is
 *          a single byte.
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_ping(void)
{
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_PING, NULL, 0);
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_download
 *
 * @brief   The COMMAND_DOWNLOAD command is sent to the bootloader to indicate
 *          where to store data in flash and how many bytes will be sent by the
 *          COMMAND_SEND_DATA commands that follow. The command consists of two
 *          32-bit values that are both transferred MSB first. The first 32-bit
 *          value is the address to start programming data into, while the second
 *          is the 32-bit size of the data that will be sent.
 *          This command must be followed by a COMMAND_GET_STATUS command to ]
 *          ensure that the program address and program size are valid for the
 *          device.
 *
 * @param   addr - Image address in internal flash
 * @param   size - Size of the image
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_download(uint32_t addr, uint32_t size)
{
  uint8_t payload[8];

  // Check input arguments
  if (size & 0x03)
  {
    // NEED TO CHECK: addr in range of device
    // CHECKING: byte count must be multiple of 4
    return SBL_FAILURE;
  }

  // Initialize payload - MSB order
  SBL_CMD_uint32MSB(addr, &payload[0]);
  SBL_CMD_uint32MSB(size, &payload[4]);

  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_DOWNLOAD, payload, sizeof(payload));
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_getStatus
 *
 * @brief   The COMMAND_GET_STATUS command returns the status of the last
 *          command that was issued.
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_getStatus(void)
{
  uint8_t rsp[1];
  uint16_t rspLen = 0;

  // Send Get Status command
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_GET_STATUS, NULL, 0);
  }

  // Get the status response from target
  SBL_TL_getRsp(rsp, sizeof(rsp), &rspLen);

  return rsp[0];
}

/**
 * @fn      SBL_CMD_sendData
 *
 * @brief   The COMMAND_SEND_DATA command must only follow a COMMAND_DOWNLOAD
 *          command or another COMMAND_SEND_DATA command, if more data is needed.
 *          Consecutive COMMAND_SEND_DATA commands automatically increment the
 *          address and continue programming from the previous location
 *
 * @param   pData - Pointer to data to send
 * @param   len   - length of buffer
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_sendData(uint8_t *pData, uint16_t len)
{
  // Check input arguments
  if (len > SBL_MAX_BYTES_PER_TRANSFER)
  {
    // Length of bytes excees maximum allowed per transfer
    return SBL_FAILURE;
  }
  // Send command
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_SEND_DATA, pData, len);
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_reset
 *
 * @brief   The COMMAND_RESET command tells the bootloader to perform a system
 *          reset.
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_reset(void)
{
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_RESET, NULL, 0);
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_sectorErase
 *
 * @brief   The COMMAND_SECTOR_ERASE command erases a specified flash sector.
 *          One flash sector has the size of 4KB.
 *
 * @param   addr - start address of the sector to erase
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_sectorErase(uint32_t addr)
{
  uint8_t payload[4];

  // Initialize payload - MSB order
  SBL_CMD_uint32MSB(addr, &payload[0]);

  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_SECTOR_ERASE, payload, sizeof(payload));
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_crc32
 *
 * @brief   The COMMAND_CRC32 command checks a flash area using CRC32.
 *
 * @param   addr - address in memory from where the CRC32 calculation starts
 * @param   size - number of bytes comprised by the CRC32 calculation
 * @param   readRepeatCnt - number of read repeats for each data location
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_crc32(uint32_t addr, uint32_t size, uint32_t readRepeatCnt)
{
  uint8_t payload[12];

  // Initialize payload - MSB order
  SBL_CMD_uint32MSB(addr, &payload[0]);
  SBL_CMD_uint32MSB(size, &payload[4]);
  SBL_CMD_uint32MSB(readRepeatCnt, &payload[8]);

  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_CRC32, payload, sizeof(payload));
  }
  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_memoryRead
 *
 * @brief   This command reads a specified number of elements with a specified
 *          access type (8- or 32 bits) from a specified memory mapped start
 *          address and returns the read data in a separate communication packet.
 *
 * @param   addr - address in memory from where the CRC32 calculation starts
 * @param   numAccess - number of 8 bit reads to do
 * @param   readRsp - pointer to buffer for read data
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_memoryRead(uint32_t addr, uint8_t numAccess, uint8_t *readRsp)
{
  uint8_t payload[6];
  uint16_t rspLen = 0;


  // Initialize payload - MSB order
  SBL_CMD_uint32MSB(addr, &payload[0]);

  // Verify we do not exceed the max reads
  if (numAccess > 253)
  {
    numAccess = 253;
  }

  // 8 bit read access
  payload[4] = 0;
  payload[5] = numAccess;

  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_MEMORY_READ, payload, sizeof(payload));
  }

  return(SBL_TL_getRsp(readRsp, numAccess, &rspLen));
}

/**
 * @fn      SBL_CMD_getChipID
 *
 * @brief   The COMMAND_GET_CHIP_ID command makes the bootloader return the
 *          value of the 32-bit user ID from the AON_WUW JTAGUSERCODE register.
 *
 * @return  uint32_t - chipID
 */
uint32_t SBL_CMD_getChipID(void)
{
  uint8_t rsp[4];
  uint16_t rspLen = 0;
  uint32_t chipID = 0;

  // Send Get Chip ID command
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_GET_CHIP_ID, NULL, 0);
  }

  // Get the status response from target
  SBL_TL_getRsp(rsp, sizeof(rsp), &rspLen);

  // Reverse MSB order of response to get Chip ID
  SBL_CMD_uint32MSB(chipID, rsp);

  return chipID;
}

/**
 * @fn      SBL_CMD_bankErase
 *
 * @brief   This command erases all
 *          main bank flash sectors including CCFG not protected by write-protect
 *          bits in FCFG1 and CCFG
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_bankErase(void)
{
  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_BANK_ERASE, NULL, 0);
  }

  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_setCCFG
 *
 * @brief   The COMMAND_SET_CCFG command is sent to the bootloader to configure
 *          the defined fields in the lash CCFG area that are read by the ROM
 *          boot FW.
 *
 * @param   fieldID - which identifies the CCFG parameter to be written
 * @param   fieldValue   - data to write
 *
 * @return  uint8_t - status of TL return
 */
uint8_t SBL_CMD_setCCFG(uint32_t fieldID, uint32_t fieldValue)
{
  uint8_t payload[8];

  // Initialize payload - MSB order
  SBL_CMD_uint32MSB(fieldID, &payload[0]);
  SBL_CMD_uint32MSB(fieldValue, &payload[4]);

  uint8_t tlSendStatus = SBL_FAILURE;
  while (SBL_FAILURE == tlSendStatus)
  {
    tlSendStatus = SBL_TL_sendCmd(SBL_CMD_SET_CCFG, payload, sizeof(payload));
  }

  return tlSendStatus;
}

/**
 * @fn      SBL_CMD_uint32MSB
 *
 * @brief   Pack a uint32 into the first 4B of a byte array
 *
 * @param   src - 32 bit value to pack into byte array
 * @param   pDst   - pointer to array to pack in
 *
 * @return  None.
 */
void SBL_CMD_uint32MSB(uint32_t src, uint8_t *pDst)
{
  // MSB first
  pDst[0] = (uint8_t)(src >> 24);
  pDst[1] = (uint8_t)(src >> 16);
  pDst[2] = (uint8_t)(src >> 8);
  pDst[3] = (uint8_t)(src >> 0);
}
